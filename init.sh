# Si se desea montar los directorios de diferentes partes del host
# deben agregarse y modificarse los volumenes. 
#
# Ejemplo:
#
# docker run ....\
#	.....\
#  	--mount type=bind,source=/path/to/storage,target=/home/jovyan/storage \


docker run -d \
	 -p 9090:8888 \
	--mount type=bind,source=$(pwd)/,target=/home/jovyan/work/ \
	--name mysetup \
	jupyter/base-notebook 


