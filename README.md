# Mysetup

Conjunto de configuraciones para procesamiento de datos en entorno Docker.

La idea de este repositorio es mantener en un lugar una configuración básica para en poco tiempo comenzar en cualquier entorno poder procesar datos. Además, se propone ua estructura de repositorio y de storage. 

----

## Dockerfile

La imagen de docker es la correspondiente a las [Jupyter Docker Stacks](https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html). El motivo de usar docker es que se puede mantener una configuración estandar independiente del host donde se levante el entorno. De esta forma puede ser de forma local, en un entorno de desarrollo/producción. 

En la pagina se pueden ver todas las opciones de imagenes, desde las más enfocadas a Ciencia de Datos o procesamientos de Data Enginieree. 

### Deploy

La imágen junto con las configuraciones de *puertos*, *volumenes*, *redes*; entre otras, se puede levantar mediante:

```
docker run -d \ 
        -p 9090:8888 \  
        --mount type=bind,source=$(pwd)/,target=/home/jovyan/ work/ \
        --name mysetup \
        jupyter/base-notebook 
```
y luego ingresar a *https://localhost:8888* donde se debe ingresar la *pass o el token*

En el repositorio he incluido un archivo **init.sh** el cual desde un entorno *gnu/linux* podría levantar la aplicación con solo ejecutar:

`
$ bash init.sh
`
En un entorno windows *solo Dios sabe*.

### Token y passs

Se puede agregar configuraciones extra según la documentación de las imágenes de docker, como también revisar los logs del container 

`
$ docker logs mysetup
`
Al final debe aparecer el token junto con la url.


